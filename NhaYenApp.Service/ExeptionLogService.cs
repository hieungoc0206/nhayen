﻿using Data.Repository;
using NhaYenApp.Data.Entity;
using NhaYenApp.Data.Infrastructure;
using System.Collections.Generic;

namespace NhaYenApp.Service
{
    public interface IExeptionLogService
    {
        void Insert(ExeptionLog entity);

        IEnumerable<ExeptionLog> GetAll();

        void Update(ExeptionLog entity);

        ExeptionLog GetById(int Id);

        void Save();
    }

    public class ExeptionLogService : IExeptionLogService
    {
        private readonly IExeptionLogRepository _exeptionLogRepository;
        private IUnitOfWork _unitOfWork;

        public ExeptionLogService(IExeptionLogRepository exeptionLogRepository, IUnitOfWork unitOfWork) => (_exeptionLogRepository, _unitOfWork) = (exeptionLogRepository, unitOfWork);

        public void Insert(ExeptionLog entity) => _exeptionLogRepository.Add(entity);

        public IEnumerable<ExeptionLog> GetAll() => _exeptionLogRepository.GetAll();

        public void Update(ExeptionLog entity) => _exeptionLogRepository.Update(entity);

        public void Save() => _unitOfWork.Commit();

        public ExeptionLog GetById(int Id) => _exeptionLogRepository.GetSingleById(Id);
    }
}