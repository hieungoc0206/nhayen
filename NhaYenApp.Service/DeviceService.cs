﻿using Data.Repository;
using NhaYenApp.Data.Entity;
using NhaYenApp.Data.Infrastructure;
using System.Collections.Generic;

namespace NhaYenApp.Service
{
    public interface IDeviceService
    {
        void Insert(Device entity);

        IEnumerable<Device> GetAll();

        void Update(Device entity);

        Device GetById(int Id);

        void Save();
    }

    public class DeviceService : IDeviceService
    {
        private readonly IDeviceRepository _deviceRepository;
        private IUnitOfWork _unitOfWork;

        public DeviceService(IDeviceRepository deviceRepository, IUnitOfWork unitOfWork) => (_deviceRepository, _unitOfWork) = (deviceRepository, unitOfWork);

        public void Insert(Device entity) => _deviceRepository.Add(entity);

        public IEnumerable<Device> GetAll() => _deviceRepository.GetAll();

        public void Update(Device entity) => _deviceRepository.Update(entity);

        public void Save() => _unitOfWork.Commit();

        public Device GetById(int Id) => _deviceRepository.GetSingleById(Id);
    }
}