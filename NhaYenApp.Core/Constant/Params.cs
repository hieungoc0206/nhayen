﻿using System.Collections.Generic;

namespace NhaYenApp.Core.Constant
{
  
    public class Params
    {
        private static Params _instance;
        #region Khởi tạo Singleton đảm bảo tại một thời điểm chỉ có duy nhất một thể hiện class được tạo ra
        protected Params()
        {
        }
        public static Params Instance()
        {
            // nếu chưa có thì tạo thể hiện duy nhất
            if (_instance == null)
                _instance = new Params();

            return _instance;
        }
        #endregion
       public class ThamSo
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public List<ThamSo> List()
        {
            List<ThamSo> lst = new List<ThamSo>();
            lst.Add(new ThamSo { Id = 1, Name = " Nồng độ PH" });
            lst.Add(new ThamSo { Id = 2, Name = "Độ mặn" });
            lst.Add(new ThamSo { Id = 3, Name = "Nồng độ Oxy" });
            lst.Add(new ThamSo { Id = 4, Name = "Nhiệt độ" });
            lst.Add(new ThamSo { Id = 5, Name = "Tỉ lệ H2S/Sulfide" });
            lst.Add(new ThamSo { Id = 6, Name = "Nồng độ NH3/NH4+" });
            lst.Add(new ThamSo { Id = 7, Name = "NH4+ Min" });
            lst.Add(new ThamSo { Id = 8, Name = "NH4+ Max" });
            lst.Add(new ThamSo { Id = 9, Name = "NO2 Min" });
            lst.Add(new ThamSo { Id = 10, Name = "NO2 Max" });
            lst.Add(new ThamSo { Id = 11, Name = "Sulfide Min" });
            lst.Add(new ThamSo { Id = 12, Name = "Sulfide Max" });
            return lst;
        }
    }
}