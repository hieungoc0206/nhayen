﻿namespace NhaYenApp.Core.Constant
{
    public class CommonConstants
    {
        // Group User
        public const int Administrator = 1;
        public const int HoDan = 2;

        // Tham so cam bien 
        public const string PH = "PH";
        public const string Slat = "Salt";
        public const string Oxy = "Oxy";
        public const string Temp = "Temp";
        public const string H2S = "H2S";
        public const string Nh3 = "NH3";
        public const string NH4Min = "NH4Min";
        public const string NH4Max = "NH4Max";
        public const string NO2Min = "NO2Min";
        public const string NO2Max = "NO2Max";
        public const string SulfideMin = "SulfideMin";
        public const string SulfideMax = "SulfideMax";
    }
}