﻿using System.Net.Mail;

namespace NhaYenApp.Common
{
    public class MailHelper
    {
        public static bool SendMail(string toEmail, string Subject, string Content)
        {
            try
            {
                var host = "smtp.gmail.com";
                var port = 587;
                var fromEmail = "bluestar.internationalclinic@gmail.com";
                var password = "ngochieu96";
                var fromName = "Sanslab WISUN Team";

                var smtpClient = new SmtpClient(host, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromEmail, password),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = true,
                    Timeout = 100000
                };

                var mail = new MailMessage
                {
                    Body = Content,
                    Subject = Subject,
                    From = new MailAddress(fromEmail, fromName)
                };

                mail.To.Add(new MailAddress(toEmail));
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                smtpClient.Send(mail);
                return true;
            }
            catch (SmtpException ex)
            {
                return false;
            }
        }
    }
}