﻿using AutoMapper;
using NhaYenApp.Data.Entity;
using NhaYenApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NhaYenApp.MobileApi
{
    [RoutePrefix("/api/datapackage")]
    public class DataPackageController : ApiController
    {
        private IDataPackageService _dataPackageService;
        public DataPackageController(IDataPackageService dataPackageService)
        {
            _dataPackageService = dataPackageService;
        }

        [Route("report")]
        [HttpGet]
        public HttpResponseMessage Report(HttpRequestMessage request, DateTime date, int deviceId, int paramId)
        {

            HttpResponseMessage response = null;
            IEnumerable<Param> lst = _dataPackageService.ReportDataPackage(deviceId, date, paramId);          
            response = request.CreateResponse(HttpStatusCode.OK, lst);
            return response;

        }


        [Route("getdatapackage")]
        public HttpResponseMessage GetParamnewest(HttpRequestMessage request, int deviceId)
        {
            HttpResponseMessage response = null;
            var model = _dataPackageService.GetParamnewest(deviceId);
            response = request.CreateResponse(HttpStatusCode.OK, model);
            return response;
        }
    }
}
