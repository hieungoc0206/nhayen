﻿using NhaYenApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NhaYenApp.MobileApi
{
    [Authorize]
    [RoutePrefix("api/device")]
    public class DeviceController : ApiController
    {
        private IDeviceService _deviceService;
        public DeviceController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }

        [Route("getall")]
        [HttpGet]       
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {         
                HttpResponseMessage response = null;
                var lstDevice = _deviceService.GetAll();
                response = request.CreateResponse(HttpStatusCode.OK, lstDevice);
                return response;
            
        }
    }
}
