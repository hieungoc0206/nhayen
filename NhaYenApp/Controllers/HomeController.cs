﻿using NhaYenApp.Common;
using NhaYenApp.Service;
using NhaYenApp.Web.Models;
using System.Linq;
using System.Web.Mvc;

namespace NhaYenApp.Controllers
{
    public class HomeController : Controller
    {
        private IDataPackageService _dataPackageService;
        private IDeviceService _deviceService;

        public HomeController(IDataPackageService dataPackageService, IDeviceService deviceService) => (_dataPackageService, _deviceService) = (dataPackageService, deviceService);

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                //send mail
                string content = System.IO.File.ReadAllText(Server.MapPath("/Content/sendMail.html"));
                content = content.Replace("{{Subject}}", model.Subject);
                content = content.Replace("{{Name}}", model.Name);
                content = content.Replace("{{Email}}", model.Email);
                content = content.Replace("{{ContentEmail}}", model.Content);

                var adminEmail = "hieunguyenngoc0206@gmail.com";
                bool result = MailHelper.SendMail(adminEmail, model.Subject, content);
                ViewBag.Message = "Thông tin liên hệ của bạn đã được gửi đi";
                return View();
            }
            return View(model);
        }

        public JsonResult GetDevice()
        {
            var device = _deviceService.GetAll();
            return Json(device.Select(x=> new { x.Id , x.Name}).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataPackage(int deviceId)
        {
            var datapackage = _dataPackageService.GetTop1000(deviceId).ToList();
            return Json(new { data = datapackage }, JsonRequestBehavior.AllowGet);
        }
    }
}