﻿using System;

namespace NhaYenApp.Web.Models
{
    public class ContactViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Subject { get; set; }

        public DateTime SendDate { get; set; }

        public string Content { get; set; }
    }
}