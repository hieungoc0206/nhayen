namespace NhaYenApp.Data.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using NhaYenApp.Core.Constant;
    using NhaYenApp.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NhaYenApp.Data.NhaYenDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(NhaYenApp.Data.NhaYenDbContext context)
        {
            CreateUser(context);
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }

        private void CreateUser(NhaYenDbContext context)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new NhaYenDbContext()));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new NhaYenDbContext()));

            var user = new ApplicationUser()
            {
                UserName = "hieungoc",
                Email = "hieunguyenngoc0206@gmail.com",
                EmailConfirmed = true,
                FullName = "Nguyen Ngoc Hieu",
                ApplicationGroupId = CommonConstants.Administrator,
                HoDanId = 1,
            };
            if (manager.Users.Count(x => x.UserName == "hieungoc") == 0)
            {
                manager.Create(user, "ngochieu96");
            }
        }

    }
}