namespace NhaYenApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initv2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataPackage", "Hum", c => c.Double(nullable: false));
            AddColumn("dbo.DataPackage", "Temp", c => c.Double(nullable: false));
            DropColumn("dbo.DataPackage", "PH");
            DropColumn("dbo.DataPackage", "Humi");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPackage", "Humi", c => c.Double(nullable: false));
            AddColumn("dbo.DataPackage", "PH", c => c.Double(nullable: false));
            DropColumn("dbo.DataPackage", "Temp");
            DropColumn("dbo.DataPackage", "Hum");
        }
    }
}
