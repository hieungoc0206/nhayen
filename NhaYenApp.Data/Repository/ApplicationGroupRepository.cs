﻿using NhaYenApp.Data.Entity;
using NhaYenApp.Data.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repository
{
    public interface IApplicationGroupRepository : IRepository<ApplicationGroup>
    {
        IEnumerable<ApplicationUser> GetListUserByGroupId(int groupId);
    }

    public class ApplicationGroupRepository : RepositoryBase<ApplicationGroup>, IApplicationGroupRepository
    {
        public ApplicationGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ApplicationUser> GetListUserByGroupId(int groupId)
        {
            var query = from u in DbContext.Users
                        where u.ApplicationGroupId.Equals(groupId)
                        select u;
            return query;
        }
    }
}