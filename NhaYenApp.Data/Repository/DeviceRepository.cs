﻿using NhaYenApp.Data.Entity;
using NhaYenApp.Data.Infrastructure;

namespace Data.Repository
{
    public interface IDeviceRepository : IRepository<Device>
    {
    }

    public class DeviceRepository : RepositoryBase<Device>, IDeviceRepository
    {
        public DeviceRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}