﻿using NhaYenApp.Data.Entity;
using NhaYenApp.Data.Infrastructure;

namespace Data.Repository
{
    public interface IExeptionLogRepository : IRepository<ExeptionLog>
    {
    }

    public class ExeptionLogRepository : RepositoryBase<ExeptionLog>, IExeptionLogRepository
    {
        public ExeptionLogRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}