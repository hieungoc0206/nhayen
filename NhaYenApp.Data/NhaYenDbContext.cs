﻿using Microsoft.AspNet.Identity.EntityFramework;
using NhaYenApp.Data.Entity;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace NhaYenApp.Data
{
    public class NhaYenDbContext : IdentityDbContext<ApplicationUser>
    {
        public NhaYenDbContext() : base("name=NhaYenDbContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
       
        public DbSet<DataPackage> DataPackages { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<ExeptionLog> ExeptionLogs { get; set; }
        public DbSet<ApplicationGroup> ApplicationGroups { set; get; }
        public DbSet<ApplicationRole> ApplicationRoles { set; get; }
        public DbSet<ApplicationRoleGroup> ApplicationRoleGroups { set; get; }

        public static NhaYenDbContext Create()
        {
            return new NhaYenDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId }).ToTable("ApplicationUserRoles");
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId).ToTable("ApplicationUserLogins");
            builder.Entity<IdentityRole>().ToTable("ApplicationRoles");
            builder.Entity<IdentityUserClaim>().HasKey(i => i.UserId).ToTable("ApplicationUserClaims");
        }
    }
}