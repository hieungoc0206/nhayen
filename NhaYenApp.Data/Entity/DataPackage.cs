﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NhaYenApp.Data.Entity
{
    [Table("DataPackage")]
    public class DataPackage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int DeviceId { get; set; }

        public DateTime TimePackage { get; set; }

        public double Hum { get; set; }

        public double Temp { get; set; }

        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }
    }
}