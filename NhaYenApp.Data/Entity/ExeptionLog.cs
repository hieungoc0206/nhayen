﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NhaYenApp.Data.Entity
{
    [Table("ExeptionLog")]
    public class ExeptionLog
    {
        [Key]
        public int Id { set; get; }

        public string Message { set; get; }

        public string ControllerName { get; set; }

        public string StackTrace { set; get; }

        public DateTime CreatedDate { set; get; }
    }
}