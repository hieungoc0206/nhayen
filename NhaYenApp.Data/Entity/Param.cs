﻿using System;

namespace NhaYenApp.Data.Entity
{
    public class Param
    {
        public DateTime time { get; set; }

        public double value { get; set; }
    }
}