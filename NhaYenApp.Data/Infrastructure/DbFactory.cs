﻿namespace NhaYenApp.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private NhaYenDbContext dbContext;

        public NhaYenDbContext Init()
        {
            return dbContext ?? (dbContext = new NhaYenDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}