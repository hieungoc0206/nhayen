﻿namespace NhaYenApp.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}