﻿using System;

namespace NhaYenApp.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        NhaYenDbContext Init();
    }
}